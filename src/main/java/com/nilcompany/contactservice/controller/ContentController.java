package com.nilcompany.contactservice.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nilcompany.contactservice.entity.Contact;
import com.nilcompany.contactservice.service.ContactService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/contacts")
public class ContentController {
	private final ContactService contactService;

	public ContentController(ContactService userService) {
		this.contactService = userService;
	}

	@GetMapping("/users/{userId}")
	public List<Contact> getContacts(@PathVariable Long userId) {
		log.info("getContacts : " + userId);
		return contactService.getContacts(userId);
	}
}
