package com.nilcompany.contactservice.service;

import java.util.List;

import com.nilcompany.contactservice.entity.Contact;

public interface ContactService {
	public List<Contact> getContacts(Long id);
}
