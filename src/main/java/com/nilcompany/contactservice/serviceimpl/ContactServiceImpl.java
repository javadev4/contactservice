package com.nilcompany.contactservice.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.nilcompany.contactservice.entity.Contact;
import com.nilcompany.contactservice.service.ContactService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ContactServiceImpl implements ContactService {

	List<Contact> contacts = List.of(new Contact(1311L, "nilcontact@gmail.com", "nil contact", 1311L),
			new Contact(1312L, "JackMacontact@gmail.com", "Jack ma contact", 1312L),
			new Contact(1313L, "PeterContact", "Peter Contact Name", 1313L));

	@Override
	public List<Contact> getContacts(Long id) {
		log.info("User ID : " + id);
		return contacts.stream().filter(contact -> contact.getUserId().equals(id))
				.collect(Collectors.toList());
	}

}
